﻿namespace WindowsDict
{
    partial class DictForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DictForm));
            this.leftTextBox = new System.Windows.Forms.TextBox();
            this.rightTextBox = new System.Windows.Forms.TextBox();
            this.translationButton = new System.Windows.Forms.Button();
            this.zk = new System.Windows.Forms.CheckBox();
            this.gk = new System.Windows.Forms.CheckBox();
            this.cet4 = new System.Windows.Forms.CheckBox();
            this.cet6 = new System.Windows.Forms.CheckBox();
            this.ky = new System.Windows.Forms.CheckBox();
            this.toefl = new System.Windows.Forms.CheckBox();
            this.ielts = new System.Windows.Forms.CheckBox();
            this.gre = new System.Windows.Forms.CheckBox();
            this.entrans = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.zhtrans = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // leftTextBox
            // 
            this.leftTextBox.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.leftTextBox.Location = new System.Drawing.Point(3, 15);
            this.leftTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.leftTextBox.Multiline = true;
            this.leftTextBox.Name = "leftTextBox";
            this.leftTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.leftTextBox.Size = new System.Drawing.Size(692, 516);
            this.leftTextBox.TabIndex = 3;
            this.leftTextBox.Text = resources.GetString("leftTextBox.Text");
            // 
            // rightTextBox
            // 
            this.rightTextBox.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rightTextBox.Location = new System.Drawing.Point(704, 15);
            this.rightTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.rightTextBox.Multiline = true;
            this.rightTextBox.Name = "rightTextBox";
            this.rightTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.rightTextBox.Size = new System.Drawing.Size(692, 516);
            this.rightTextBox.TabIndex = 4;
            // 
            // translationButton
            // 
            this.translationButton.Location = new System.Drawing.Point(1409, 479);
            this.translationButton.Margin = new System.Windows.Forms.Padding(4);
            this.translationButton.Name = "translationButton";
            this.translationButton.Size = new System.Drawing.Size(68, 54);
            this.translationButton.TabIndex = 5;
            this.translationButton.Text = "翻译";
            this.translationButton.UseVisualStyleBackColor = true;
            this.translationButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // zk
            // 
            this.zk.AutoSize = true;
            this.zk.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zk.Location = new System.Drawing.Point(1413, 36);
            this.zk.Margin = new System.Windows.Forms.Padding(4);
            this.zk.Name = "zk";
            this.zk.Size = new System.Drawing.Size(66, 22);
            this.zk.TabIndex = 6;
            this.zk.Tag = "zk";
            this.zk.Text = "中考";
            this.zk.UseVisualStyleBackColor = true;
            // 
            // gk
            // 
            this.gk.AutoSize = true;
            this.gk.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gk.Location = new System.Drawing.Point(1413, 64);
            this.gk.Margin = new System.Windows.Forms.Padding(4);
            this.gk.Name = "gk";
            this.gk.Size = new System.Drawing.Size(66, 22);
            this.gk.TabIndex = 7;
            this.gk.Tag = "gk";
            this.gk.Text = "高考";
            this.gk.UseVisualStyleBackColor = true;
            // 
            // cet4
            // 
            this.cet4.AutoSize = true;
            this.cet4.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cet4.Location = new System.Drawing.Point(1413, 91);
            this.cet4.Margin = new System.Windows.Forms.Padding(4);
            this.cet4.Name = "cet4";
            this.cet4.Size = new System.Drawing.Size(66, 22);
            this.cet4.TabIndex = 8;
            this.cet4.Tag = "cet4";
            this.cet4.Text = "四级";
            this.cet4.UseVisualStyleBackColor = true;
            // 
            // cet6
            // 
            this.cet6.AutoSize = true;
            this.cet6.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.cet6.Location = new System.Drawing.Point(1413, 119);
            this.cet6.Margin = new System.Windows.Forms.Padding(4);
            this.cet6.Name = "cet6";
            this.cet6.Size = new System.Drawing.Size(66, 22);
            this.cet6.TabIndex = 9;
            this.cet6.Tag = "cet6";
            this.cet6.Text = "六级";
            this.cet6.UseVisualStyleBackColor = true;
            // 
            // ky
            // 
            this.ky.AutoSize = true;
            this.ky.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ky.Location = new System.Drawing.Point(1413, 146);
            this.ky.Margin = new System.Windows.Forms.Padding(4);
            this.ky.Name = "ky";
            this.ky.Size = new System.Drawing.Size(66, 22);
            this.ky.TabIndex = 10;
            this.ky.Tag = "ky";
            this.ky.Text = "考研";
            this.ky.UseVisualStyleBackColor = true;
            // 
            // toefl
            // 
            this.toefl.AutoSize = true;
            this.toefl.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toefl.Location = new System.Drawing.Point(1413, 174);
            this.toefl.Margin = new System.Windows.Forms.Padding(4);
            this.toefl.Name = "toefl";
            this.toefl.Size = new System.Drawing.Size(66, 22);
            this.toefl.TabIndex = 11;
            this.toefl.Tag = "toefl";
            this.toefl.Text = "托福";
            this.toefl.UseVisualStyleBackColor = true;
            // 
            // ielts
            // 
            this.ielts.AutoSize = true;
            this.ielts.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ielts.Location = new System.Drawing.Point(1413, 201);
            this.ielts.Margin = new System.Windows.Forms.Padding(4);
            this.ielts.Name = "ielts";
            this.ielts.Size = new System.Drawing.Size(66, 22);
            this.ielts.TabIndex = 12;
            this.ielts.Tag = "ielts";
            this.ielts.Text = "雅思";
            this.ielts.UseVisualStyleBackColor = true;
            // 
            // gre
            // 
            this.gre.AutoSize = true;
            this.gre.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gre.Location = new System.Drawing.Point(1413, 229);
            this.gre.Margin = new System.Windows.Forms.Padding(4);
            this.gre.Name = "gre";
            this.gre.Size = new System.Drawing.Size(57, 22);
            this.gre.TabIndex = 13;
            this.gre.Tag = "gre";
            this.gre.Text = "GRE";
            this.gre.UseVisualStyleBackColor = true;
            // 
            // entrans
            // 
            this.entrans.AutoSize = true;
            this.entrans.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.entrans.Location = new System.Drawing.Point(1409, 309);
            this.entrans.Margin = new System.Windows.Forms.Padding(4);
            this.entrans.Name = "entrans";
            this.entrans.Size = new System.Drawing.Size(102, 22);
            this.entrans.TabIndex = 14;
            this.entrans.Tag = "entrans";
            this.entrans.Text = "英文翻译";
            this.entrans.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::WindowsDict.Properties.Resources.m21;
            this.pictureBox1.Location = new System.Drawing.Point(1405, 354);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(129, 118);
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1431, 335);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 16;
            this.label1.Text = "打赏我";
            // 
            // zhtrans
            // 
            this.zhtrans.AutoSize = true;
            this.zhtrans.Checked = true;
            this.zhtrans.CheckState = System.Windows.Forms.CheckState.Checked;
            this.zhtrans.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.zhtrans.Location = new System.Drawing.Point(1409, 279);
            this.zhtrans.Margin = new System.Windows.Forms.Padding(4);
            this.zhtrans.Name = "zhtrans";
            this.zhtrans.Size = new System.Drawing.Size(102, 22);
            this.zhtrans.TabIndex = 17;
            this.zhtrans.Tag = "zhtrans";
            this.zhtrans.Text = "中文翻译";
            this.zhtrans.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 535);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(829, 15);
            this.label2.TabIndex = 18;
            this.label2.Text = "词典库共3320061条(包含词语和短句)，库来源于网络，每个词翻译的准确性未进行过人工校验，请结合百度翻译配合使用！";
            // 
            // DictForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1551, 558);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.zhtrans);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.entrans);
            this.Controls.Add(this.gre);
            this.Controls.Add(this.ielts);
            this.Controls.Add(this.toefl);
            this.Controls.Add(this.ky);
            this.Controls.Add(this.cet6);
            this.Controls.Add(this.cet4);
            this.Controls.Add(this.gk);
            this.Controls.Add(this.zk);
            this.Controls.Add(this.translationButton);
            this.Controls.Add(this.rightTextBox);
            this.Controls.Add(this.leftTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "DictForm";
            this.Text = "ijk英语--www.52en.me";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox leftTextBox;
        private System.Windows.Forms.TextBox rightTextBox;
        private System.Windows.Forms.Button translationButton;
        private System.Windows.Forms.CheckBox zk;
        private System.Windows.Forms.CheckBox gk;
        private System.Windows.Forms.CheckBox cet4;
        private System.Windows.Forms.CheckBox cet6;
        private System.Windows.Forms.CheckBox ky;
        private System.Windows.Forms.CheckBox toefl;
        private System.Windows.Forms.CheckBox ielts;
        private System.Windows.Forms.CheckBox gre;
        private System.Windows.Forms.CheckBox entrans;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox zhtrans;
        private System.Windows.Forms.Label label2;
    }
}

